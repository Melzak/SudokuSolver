import time
from typing import Union, List

from tqdm import tqdm

from classes.creater_empty_signs import BlankCreator
from classes.solver import SudokuSolver
import numpy as np


class Simulator:
    def __init__(self):
        self._blankcreator = BlankCreator()
        self.solver = SudokuSolver()

    def load_list(self, board: List[List[Union[int, str]]]):
        self._blankcreator.read_from_list(board)

    def simulate(self, num_of_blanks, tries):
        assert not self._blankcreator.isempty()

        solved = 0
        start = time.time()
        prog = tqdm(range(tries), colour="BLACK", desc=f"{num_of_blanks} blanks")
        for _ in prog:
            self.solver.load_array(self._blankcreator.create_blanks(num_of_blanks))
            solved += self.solver.solve()

        # return {no_blanks: (solved/tries)*100.}
        return {num_of_blanks: tries / (prog.last_print_t - start)}