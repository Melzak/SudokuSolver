from copy import copy
from random import randint
from classes.sudoku import Sudoku
from examples import example2


class BlankCreator(Sudoku):
    @staticmethod
    def _count(board):
        return sum(1 for row in board for val in row if not val)

    def create_blanks(self, num_of_blanks):
        assert self.board is not None

        copy_board = copy(self.board)

        for _ in range(num_of_blanks):
            row_idx = randint(0, 8)
            column_idx = randint(0, 8)

            copy_board[row_idx][column_idx] = 0

        return copy_board

    def isempty(self):
        if self.board is None:
            return True

        return False


if __name__ == '__main__':
    sudoku = BlankCreator()
    sudoku.read_from_list(example2)
    sudoku.create_blanks(5)
    print(sudoku)
