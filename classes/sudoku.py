from typing import List, Union
from numpy import array
from examples import example2


class Sudoku:
    banned_empty_signs = [".", "", " "]

    def __init__(self):
        self.board = None
        self.num_counter = None
        self.num_of_empty = 0

    def read_array(self, board: array):
        self.board = board

    def read_from_list(self, board: List[List[Union[str, int]]]):
        self.board = self.list_adapter(board)

    def list_adapter(self, board: List[List[Union[str, int]]]):
        board = [[int(val) if val not in self.banned_empty_signs else 0 for val in row] for row in board]
        return array(board)

    def __repr__(self):
        s = []
        for i, row in enumerate(self.board):
            formated_row = []
            for num in range(3):
                formated_row.append(" ".join(str(val) for val in row[num * 3: (num + 1) * 3]))
            s.append(" | ".join(formated_row))
            if i % 3 == 2 and i < 8:
                s.append("-".join("" for _ in range(22)))
        return "\n".join(s)

    def __str__(self):
        s = []
        for i, row in enumerate(self.board):
            formated_row = []
            for num in range(3):
                formated_row.append(" ".join(str(val) for val in row[num * 3: (num + 1) * 3]))
            s.append(" | ".join(formated_row))
            if i % 3 == 2 and i < 8:
                s.append("-".join("" for _ in range(22)))

        return "\n".join(s)


if __name__ == '__main__':
    sudoku = Sudoku()
    sudoku.read_from_list(example2)
    print(sudoku)
