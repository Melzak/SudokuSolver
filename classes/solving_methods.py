from abc import abstractmethod
from copy import copy

from classes.sudoku import Sudoku

NUMBERS_IN_SECTION = set(range(1, 10))


class SolvingMethods:
    def __init__(self):
        self.sudoku = Sudoku()
        self.indexes_of_numbers = None

        self.solve_methods = [
            self.merge_method,
        ]

    def _get_all_nums(self):
        self.indexes_of_numbers = {num: set() for num in range(10)}
        for x in range(9):
            for y in range(9):
                self.indexes_of_numbers[self.sudoku.board[x, y]].add((x, y))

    def __get_collumn(self, indexes):
        x, y = indexes
        column = set(self.sudoku.board[:, y])
        return NUMBERS_IN_SECTION.difference(column)

    def __get_row(self, indexes):
        x, y = indexes
        row = set(self.sudoku.board[x, :])
        return NUMBERS_IN_SECTION.difference(row)

    def __get_part(self, indexes):
        x, y = indexes
        section_x = (x // 3) * 3
        section_y = (y // 3) * 3
        section = self.sudoku.board[section_x:section_x + 3, section_y:section_y + 3]
        section = {elem for row in section for elem in row}
        return NUMBERS_IN_SECTION.difference(section)

    def merge_method(self, indexes):
        section = self.__get_part(indexes)
        column = self.__get_collumn(indexes)
        row = self.__get_row(indexes)
        return section.intersection(column, row)

    def force_method(self):
        self._get_all_nums()
        board_copy = copy(self.sudoku.board)
        for x, y in self.indexes_of_numbers[0]:
            posible_nums = self.merge_method((x, y))
            if not posible_nums:
                return False
            for num in posible_nums:
                self.sudoku.board[x, y] = num

                if self.solve():
                    return True

                self.sudoku.board = copy(board_copy)

            return False
        return True

    def _force_method2(self):
        self._get_all_nums()
        for x, y in self.indexes_of_numbers[0]:
            posible_nums = self.merge_method((x, y))

            for num in posible_nums:
                self.sudoku.board[x, y] = num

                if self.force_method():
                    return True

                self.sudoku.board[x,y] = 0

            return False

        return True

    @abstractmethod
    def solve(self):
        pass
