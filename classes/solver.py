import time
from copy import copy
import matplotlib.pyplot as plt
from numpy import array
from tqdm import tqdm

from classes.creater_empty_signs import BlankCreator
from classes.solving_methods import SolvingMethods
from examples import example1
from classes.sudoku import Sudoku


class SudokuSolver(SolvingMethods):
    def load_array(self, board: array):
        self.sudoku.read_array(board)
        self._get_all_nums()

    def load_sudoku(self, sudoku: Sudoku):
        if not isinstance(sudoku, Sudoku):
            raise Exception

        self.sudoku = sudoku
        self._get_all_nums()

    def check_method(self, method, x, y):
        section = method((x, y))
        if len(section) == 1:
            val, = section
            self.sudoku.board[x, y] = val
            return True
        elif not section:
            return None

        return False

    def solve(self):
        assert self.sudoku.board is not None
        self._get_all_nums()
        change = True
        while change and len(self.indexes_of_numbers[0]) > 0:
            change = False
            for x, y in self.indexes_of_numbers[0]:
                for method in self.solve_methods:
                    change = self.check_method(method, x, y)
                    if change:
                        break
                    elif change is None:
                        return False

            self._get_all_nums()

        if not self.validate():
            if self.force_method():
                return self.validate()

            return False

        return True

    def validate(self):
        assert self.sudoku is not None
        if len(self.indexes_of_numbers[0]):
            return False

        for num in range(1, 10):
            if len(self.indexes_of_numbers[num]) != 9:
                return False

            x_indexes = set(x for x, y in self.indexes_of_numbers[num])
            if len(x_indexes) != 9:
                return False
            y_indexes = set(y for x, y in self.indexes_of_numbers[num])

            if len(y_indexes) != 9:
                return False

        return True


if __name__ == '__main__':
    sudoku = BlankCreator()
    tries = 10000
    blanks = 54
    solver = SudokuSolver()
    time_1 = 0
    time_2 = 0
    sudoku.read_from_list(example1)
    for num in tqdm(range(tries)):
        sudoku.create_blanks(blanks)
        solver.load_array(copy(sudoku.board))
        start = time.time()
        logic1 = solver.solve()
        time_1 += time.time() - start

    print(time_1/tries)