import matplotlib.pyplot as plt
from tqdm import tqdm

import examples
from classes.simulator import Simulator

def main():
    tries = 100000
    simulator = Simulator()
    simulator.load_list(examples.example1)
    for blanks in range(70):
        yield simulator.simulate(blanks, tries)

    return dict()

if __name__ == '__main__':
    to_plot = {}

    for results in main():
        to_plot.update(results)
    plt.bar(to_plot.keys(), to_plot.values())
    plt.show()
